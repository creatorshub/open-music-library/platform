@php
    $baseUrl = "https://creatorshub-community.ams3.digitaloceanspaces.com/assets";
    $logoUrl = [
        'light' => $baseUrl . '/logos/1x/logo_vertical_white.png',
        'dark' => $baseUrl . '/logos/1x/logo_vertical_color.png',
    ];
@endphp
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>Music Library | CreatorsHub</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ $baseUrl }}/landing-page/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
    <link href="{{ $baseUrl }}/landing-page/css/socicon.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ $baseUrl }}/landing-page/css/iconsmind.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ $baseUrl }}/landing-page/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ $baseUrl }}/landing-page/css/stack-interface.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ $baseUrl }}/landing-page/css/theme.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ $baseUrl }}/landing-page/css/custom.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
    <style>
        .bar .logo {
            max-height: 2.5em;
            top: 2px;
        }
    </style>
</head>
<body data-smooth-scroll-offset="77">
    <div class="nav-container">
        <div class="via-1533141553349">
            <div class="bar bg--dark bar--sm visible-sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <a href="{{ url('/') }}">
                                <img class="logo logo-dark" alt="logo" src="{{ $logoUrl['dark'] }}">
                                <img class="logo logo-light" alt="logo" src="{{ $logoUrl['light'] }}">
                            </a>
                        </div>
                        <div class="col-9 col-md-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu3;hidden-xs hidden-sm">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="bar bar--sm bg--dark" id="menu3">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 hidden-xs hidden-sm order-lg-1">
                            <div class="bar__module">
                                <a href="{{ url('/') }}">
                                    <img class="logo logo-dark" alt="logo" src="{{ $logoUrl['dark'] }}">
                                    <img class="logo logo-light" alt="logo" src="{{ $logoUrl['light'] }}">
                                </a>
                            </div>
                        </div>
                        <!--div class="col-lg-2 order-lg-2">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li><a href="#">Nav</a></li>
                                </ul>
                            </div>
                        </div-->
                        <div class="col-lg-5 text-right text-left-xs order-lg-4">
                            <!--div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li><a href="#">Nav Right</a></li>
                                </ul>
                            </div-->
                            <div class="bar__module">
                                <a class="btn btn--primary btn--sm type--uppercase" href="{{ route('login') }}">
                                    <span class="btn__text">Login</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="main-container">
        <section class="cover height-80 imagebg text-center" data-overlay="5">
            <div class="background-image-holder background--top">
                <img alt="background" src="{{ $baseUrl }}/landing-page/img/landing-1.jpg">
            </div>
            <div class="container pos-vertical-center">
                <div class="row">
                    <div class="col-md-8">
                        <img alt="Image" class="unmarg--bottom" src="{{ $baseUrl }}/landing-page/img/headline-1.png">
                        <h3>FREE MUSIC FOR EVERYONE</h3>
                        <a class="btn btn--primary type--uppercase" href="{{ route('login') }}">
                            <span class="btn__text">join the community</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="switchable switchable--switch">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-md-6 col-lg-5">
                        <div class="switchable__text">
                            <div class="switchable__text">
                                <h2>Easy - Search &amp; Discovery</h2>
                                <p class="lead">
                                    You know what you want? Just search and press download.
                                    You can also check out new releases, whats trending and
                                    the best playlist's for your genre.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-12">
                        <div class="pricing pricing-1 boxed boxed--lg boxed--border">
                            <h3>Free</h3>
                            <span class="h2"><strong>$0.00</strong></span>
                            <span class="type--fine-print">Free as in beer</span>
                            <span class="label">Free forever</span>
                            <hr>
                            <ul>
                                <li><span class="checkmark bg--primary-1"></span><span>Over 1800 Songs</span></li>
                                <li><span class="checkmark bg--primary-1"></span><span>Ad-Free Listening&nbsp;</span></li>
                                <li><span class="checkmark bg--primary-1"> </span><span>Unlimited Downloads</span></li>
                                <li><span class="checkmark bg--primary-1"></span><span>CC0</span></li>
                                <li><span class="checkmark bg--primary-1"></span><span>Royalty Free</span></li>
                            </ul>
                            <a class="btn btn--primary" href="{{ route('login') }}">
                                <span class="btn__text">Join the community</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg--secondary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="text-block">
                            <h4>Frequently Asked Questions</h4>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="text-block">
                            <h5>Can I use this music in my YouTube videos?</h5>
                            <p>Yes!</p>
                        </div>
                        <div class="text-block">
                            <h5>Will I get a Content ID claim if I use this music?</h5>
                            <p>No!</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="text-block">
                            <h5>Is it really free or are there any hidden cost?</h5>
                            <p>No!</p>
                        </div>
                        <div class="text-block">
                            <h5>Can I use this music outside of YouTube (eg Twitch)</h5>
                            <p>Yes!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="space--sm footer-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 col-xs-6">
                        <h6 class="type--uppercase">Creatorshub</h6>
                        <ul class="list--hover">
                            <li><a href="https://www.creatorshub.net/index.php?app=forums&module=forums&controller=index"
                                   target="_self">Home</a></li>
                            <li><a href="https://www.creatorshub.net/privacy/" target="_self">Privacy Policy</a></li>
                            <li><a href="https://www.creatorshub.net/legal/imprint" target="_self">Legal Notice</a></li>
                            <li><a href="https://www.creatorshub.net/contact/" target="_self">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 col-xs-6">
                        <h6 class="type--uppercase">Our Apps</h6>
                        <ul class="list--hover">
                            <li><a href="https://music.creatorshub.net/" target="_self">Music Library</a></li>
                            <li><a href="https://match.creatorshub.net/" target="_self">Collaboration Finder</a></li>
                            <li><a href="https://podcasts.creatorshub.net/" target="_self">Podcaster Service</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 col-xs-6">
                        <h6 class="type--uppercase">Communities</h6>
                        <ul class="list--hover">
                            <li>
                                <a href="https://www.creatorshub.net/index.php?app=forums&module=forums&controller=forums&id=1"
                                   target="_self">YouTube</a></li>
                            <li>
                                <a href="https://www.creatorshub.net/index.php?app=forums&module=forums&controller=forums&id=42">Twitch</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 col-xs-6">
                        <h6 class="type--uppercase">Follow Us</h6>
                        <ul class="list--hover">
                            <li>
                                <a href="//twitter.com/creatorshub" target="_blank" rel="noreferrer noopener nofollow">
                                    <i class="socicon socicon-twitter icon icon--xs"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <span class="type--fine-print">© 2018 CreatorsHub&nbsp;</span>
                    </div>
                    <div class="col-sm-6 text-right text-left-xs">
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ $baseUrl }}/landing-page/js/jquery-3.1.1.min.js"></script>
    <script src="{{ $baseUrl }}/landing-page/js/parallax.js"></script>
    <script src="{{ $baseUrl }}/landing-page/js/granim.min.js"></script>
    <script src="{{ $baseUrl }}/landing-page/js/smooth-scroll.min.js"></script>
    <script src="{{ $baseUrl }}/landing-page/js/scripts.js"></script>
</body>
</html>